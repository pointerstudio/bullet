/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2015 Google Inc. http://bulletphysics.org

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it freely,
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "MultipleBoxesGravity.h"

#include "btBulletDynamicsCommon.h"
#include "LinearMath/btVector3.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "../CommonInterfaces/CommonRigidBodyBase.h"
#include "../CommonInterfaces/CommonParameterInterface.h"

const int TOTAL_BOXES = 2000;

static btScalar gForceScalar = 5000;  // default force scalar to apply gravity
static btScalar gCenterRadius = 10;  // default force scalar to apply gravity

struct MultipleBoxesGravityExample : public CommonRigidBodyBase {
    MultipleBoxesGravityExample(struct GUIHelperInterface * helper)
        : CommonRigidBodyBase(helper){
    }
    virtual ~MultipleBoxesGravityExample(){
	}
	virtual void initPhysics();
    virtual void stepSimulation(float deltaTime);                                                                                               // step the simulation
    virtual void renderScene();
    void resetCamera(){
        float dist = 210;
		float pitch = -35;
		float yaw = 52;
		float targetPos[3] = {0, 0.46, 0};
		m_guiHelper->resetCamera(dist, yaw, pitch, targetPos[0], targetPos[1], targetPos[2]);
	}
};

void MultipleBoxesGravityExample::initPhysics(){
    {  // create a slider to apply the force by slider
        SliderParams slider("Apply gravity forces", &gForceScalar);
        slider.m_minVal = -10000;
        slider.m_maxVal = 10000;
        slider.m_clampToNotches = false;
        m_guiHelper->getParameterInterface()->registerSliderFloatParameter(
            slider);
    }
    {  // create a slider to apply the force by slider
        SliderParams slider("center radius", &gCenterRadius);
        slider.m_minVal = 0;
        slider.m_maxVal = 100;
        slider.m_clampToNotches = false;
        m_guiHelper->getParameterInterface()->registerSliderFloatParameter(
            slider);
    }
    m_guiHelper->setUpAxis(1);

	createEmptyDynamicsWorld();
    m_dynamicsWorld->setGravity(btVector3(0, 0, 0));

	m_guiHelper->createPhysicsDebugDrawer(m_dynamicsWorld);

    if(m_dynamicsWorld->getDebugDrawer()){
		m_dynamicsWorld->getDebugDrawer()->setDebugMode(btIDebugDraw::DBG_DrawWireframe + btIDebugDraw::DBG_DrawContactPoints);
    }

	///create a few basic rigid bodies
//    btBoxShape * groundShape = createBoxShape(btVector3(btScalar(50.), btScalar(50.), btScalar(50.)));
//	m_collisionShapes.push_back(groundShape);

//	btTransform groundTransform;
//	groundTransform.setIdentity();
//	groundTransform.setOrigin(btVector3(0, -50, 0));
//	{
//		btScalar mass(0.);
//		createRigidBody(mass, groundTransform, groundShape, btVector4(0, 0, 1, 1));
//	}

	{
		//create a few dynamic rigidbodies
		// Re-using the same collision is better for memory usage and performance
        btBoxShape * colShape = createBoxShape(btVector3(1, 1, 1));

		m_collisionShapes.push_back(colShape);

		/// Create Dynamic Objects
		btTransform startTransform;
		startTransform.setIdentity();

        btScalar mass(80.f);

		//rigidbody is dynamic if and only if mass is non zero, otherwise static
		bool isDynamic = (mass != 0.f);

		btVector3 localInertia(0, 0, 0);
        if(isDynamic){
			colShape->calculateLocalInertia(mass, localInertia);
        }

        btScalar radius = 200;
        for(int i = 0; i < TOTAL_BOXES; ++i){
            const btVector3 position = radius * btVector3(
                float(rand() * 2.0f / RAND_MAX) - 1,
                float(rand() * 2.0f / RAND_MAX) - 1,
                float(rand() * 2.0f / RAND_MAX) - 1
                );
            startTransform.setOrigin(position);
			createRigidBody(mass, startTransform, colShape);
		}
    }

	m_guiHelper->autogenerateGraphicsObjects(m_dynamicsWorld);
}

int n_pushPerpendicular = 2;

void MultipleBoxesGravityExample::stepSimulation(float deltaTime){
//    applyMForceWithForceScalar(gForceScalar);  // apply force defined by apply force slider

    if(m_dynamicsWorld){
//        b3Printf("nonstatics %d\n", m_dynamicsWorld->getNonStaticRigidBodies().size());
        if(n_pushPerpendicular > 0){
            b3Printf("nonstatics %d\n", m_dynamicsWorld->getNonStaticRigidBodies().size());
            for(int i = 0; i < m_dynamicsWorld->getNonStaticRigidBodies().size(); i++){
                const auto & rb = m_dynamicsWorld->getNonStaticRigidBodies().at(i);
                const btVector3 positionNormalized = rb->getWorldTransform().getOrigin().normalized();
                const btVector3 random = btVector3(
                    float(rand() * 2.0f / RAND_MAX) - 1,
                    float(rand() * 2.0f / RAND_MAX) - 1,
                    float(rand() * 2.0f / RAND_MAX) - 1
                    );
//                const btVector3 direction = btVector3(
//                    positionNormalized.y() * -1,
//                    positionNormalized.x(),
//                    positionNormalized.z()
//                    );
                const btVector3 direction = positionNormalized.cross(random);
                const btScalar force = 1000;
                rb->setActivationState(DISABLE_DEACTIVATION);
                rb->setDamping(0, 0);
                rb->setFriction(0);
                rb->applyCentralImpulse(direction.normalized() * force);
            }
            n_pushPerpendicular--;
        }else{
            for(int i = 0; i < m_dynamicsWorld->getNonStaticRigidBodies().size(); i++){
                const auto & rb = m_dynamicsWorld->getNonStaticRigidBodies().at(i);
                const btVector3 position = rb->getWorldTransform().getOrigin();
                const btScalar distance = position.length();
                if(distance > gCenterRadius){
                    const btVector3 direction = -1 * position.normalized();
                    const btScalar force = rb->getMass() / (distance * distance);
                    rb->applyCentralForce(direction * force * gForceScalar);
                }
            }

        }
        m_dynamicsWorld->stepSimulation(deltaTime);
    }
}


void MultipleBoxesGravityExample::renderScene(){
	CommonRigidBodyBase::renderScene();
}

CommonExampleInterface * ET_MultipleBoxesGravityCreateFunc(CommonExampleOptions & options){
    return new MultipleBoxesGravityExample(options.m_guiHelper);
}
